Subaru RKE Hacking
=====================================


# Motivation
While working on a project which involved spoofing TPMS packets I decided to record some transmissions from my keyless entry fob to see how it communicated with the car out of curiosity.  After recording and decoding some packets they seemed to have little to no encryption so I investigated further to see what kind of security the keyless entry remotes used.

## Summary
The keyless entry system on certain Subaru's has essentially no security and key fobs can easily be cloned by listening to a single transmission to capture the unique ID of the keyfob that is programmed to a car.  The key fob implements a simple rolling code with a 8-bit XOR checksum.  Wether this checksum is meant as a security feature is unknown, but its a trivial checksum so my assumption is that its purely a checksum and not for security purposes.  The rolling code prevents a replay attack, however it is trivial to calculate the next code and send it allowing an attacker to unlock the car.  

Attacks would work as follows.
#### Unlocking a victims car
* Record a single keypress from the victims key fob.  For example when the victim is locking their car and leaving.
   * This can be done at a large distance away from the victim.
* Send the unlock code to unlock the victims car.
   * This keypress can be sent any time in the future

#### Enabling the panic alarm for any keypress
* Record a single keypress from the victims key fob.
* Immediately send the panic signal.
   * With this attack the victim would never be able to disable the panic alarm with the keyfob and the car would have to be unlocked with the physical key to disable the alarm.

#### Disabling a victims key fob
* Record a single keypress from the victims key fob.
* Skip a large amount of codes in the future and send that code.
   * The car remembers the last valid rolling code used so by sending a code that is 100 in the future the victims key fob transmissions would be ignored/rejected for the next 100 times they pressed a button.
   * I assume the car would accept a code very far in the future effectively disabling the keyfob forever.  But I don't want to try it on my car.

### Vulnerable Car Models
I have demonstrated the vulnerability on a 2012 Subaru Impreza WRX and a 2016 Subaru Forester.  The 2012 Impreza uses CWTWB1U819 remote and the 2016 Subaru Forester uses CWTWB1U811 remote.  I therefore assume that any car using those two key fobs will be vulnerable.
* Cars that use key fob [CWTWB1U819](https://www.remotesremotes.com/product-p/sub-819-3btn.htm)
   * 2011-2012 Subaru Forester
   * 2009-2013 Subaru Impreza
   * 2011-2013 Subaru Legacy
   * 2011-2013 Subaru Outback
* Cars that use key fob [CWTWB1U811](https://www.remotesremotes.com/product-p/sub_811-82.htm)
   * 2012-2017 Subaru Forester
   * 2012-2017 Subaru Impreza
   * 2012-2017 Subaru Legacy
   * 2012-2015 Subaru WRX
   * 2013-2017 Subaru WRX STi
   * 2013-2015 Subaru XV (includes Crosstrek)
* Also see Tom Wimmenhove's Exploit of the following models here [subarufobrob](https://github.com/tomwimmenhove/subarufobrob) for a similar fault for previous models of cars
   * 2006 Subaru Baja
   * 2005 - 2010 Subaru Forester
   * 2004 - 2011 Subaru Impreza
   * 2005 - 2010 Subaru Legacy
   * 2005 - 2010 Subaru Outback

### Mitigation
The only way to prevent this style of attack is to not use the key fob.

### Preventing this in the future
Use a more robust rolling code mechanisim.  These however may still be vulnerable to attack. Ideally the key and car would communicate and share keys much like the Diffie-Hellman key exchange that occurs during ssh.  This would requie the key fob to be able to receive data and for the car to be able to transmit data, but I do not believe it would add much to the overall.

# Decoding a single packet and determining the fields
After recording the IQ data at 315MHz I determined it was ASK modulation and it appeared that the data portion was manchester encoded.  Below is a screenshot of the IQ data and demodulated data along with the decoded bits and bytes and what I believe each field in the packet is.  The below screenshot was generated using the data from [remote_key_press_0.c8](/subaru/recordings/remote_key_press_0.c8).

NOTE: I arbitrarily chose rising edge = '1'.  It may turn out later that using rising edge = '0' is the better choice.

![ScreenShot](/subaru/Examples/decoded_packet.png)

The layout of the packets is as follows
* Each button presson the key fob results in 3x identical packets being transmitted
* The packet starts with ~264 clocks preamble of alternating 0's and 1's with a period of 492&mu;s (130ms)
* A single low bit of width 125&mu;s is transmittied
* The Manchester encoded data then begins with a data bit transferred every ~250&mu;s
   * 56 data bits are transferred
* The whole packet takes 144ms to send
* The next packet begins ~1.3ms after completion of the first
* The second packet only sends 32 clocks in the preamble Then continues with the same pattern as the first packet
* The next packet begins ~1.3ms after completion of the second
* The third packet only sends 32 clocks in the preamble Then continues with the same pattern as the first packet


With the packets decoded I setup SDRSharp to acquire the AM demodulated data and then wrote a python script to convert the recorded wav data into packets with [wav_to_packet_decode.py](/subaru/scripts/wav_to_packet_decode.py).  With this script I recorded a large number of packets and kept track wether they were a lock, unlock, or trunk command.
```
Unlock	fa90aedfee555e
Unlock	fa90aedfee545d
Lock	fa90aeefee5368
Trunk	fa90ae4fee52c7
Trunk	fa90ae4fee51ca
Unlock	fa90aedfee5059
Unlock	fa90aedfee4f44
Unlock	fa90aedfee4e43
Unlock	fa90aedfee4d46
Unlock	fa90aedfee4c45
Unlock	fa90aedfee4b40
Unlock	fa90aedfee4a3f
Unlock	fa90aedfee4942
Unlock	fa90aedfee4841
Lock	fa90aeefee477c
Lock	fa90aeefee467b
Lock	fa90aeefee457e
Lock	fa90aeefee447d
Lock	fa90aeefee4378
Lock	fa90aeefee4277
Lock	fa90aeefee417a
Lock	fa90aeefee4079
Unlock	fa90aedfee3f34
Unlock	fa90aedfee3e33
Unlock	fa90aedfee3d36
Unlock	fa90aedfee3c35
Unlock	fa90aedfee3b30
Unlock	fa90aedfee3a2f
Unlock	fa90aedfee3932
Lock	fa90aeefee3801
Lock	fa90aeefee370c
Lock	fa90aeefee360b
Lock	fa90aeefee350e
Lock	fa90aeefee340d
Lock	fa90aeefee3308
Lock	fa90aeefee3207
Unlock	fa90aedfee313a
Unlock	fa90aedfee3039
Unlock	fa90aedfee2f24
Unlock	fa90aedfee2e23
Unlock	fa90aedfee2d26
Unlock	fa90aedfee2c25
Unlock	fa90aedfee2b20
Lock	fa90aeefee2a0f
Lock	fa90aeefee2912
Lock	fa90aeefee2811
Lock	fa90aeefee271c
Lock	fa90aeefee261b
Lock	fa90aeefee251e
Lock	fa90aeefee241d
Unlock	fa90aedfee2328
Unlock	fa90aedfee2227
Unlock	fa90aedfee212a
Unlock	fa90aedfee2029
Unlock	fa90aedfee1f14
Unlock	fa90aedfee1e13
Unlock	fa90aedfee1d16
Lock	fa90aeefee1c25
Lock	fa90aeefee1b20
Lock	fa90aeefee1a1f
Lock	fa90aeefee1922
Unlock	fa90aedfee1811
Lock	fa90aeefee172c
Unlock	fa90aedfee161b
Lock	fa90aeefee152e
Unlock	fa90aedfee141d
Lock	fa90aeefee1328
Unlock	fa90aedfee1217
Lock	fa90aeefee112a
Unlock	fa90aedfee1019
Lock	fa90aeefee0f34
Unlock	fa90aedfee0e03
Lock	fa90aeefee0d36
Unlock	fa90aedfee0c05
Lock	fa90aeefee0b30
Unlock	fa90aedfee0aff
Lock	fa90aeefee0932
Unlock	fa90aedfee0801
Lock	fa90aeefee073c
Unlock	fa90aedfee060b
Lock	fa90aeefee053e
... Missed some
Lock	fa90aeefedf0c8
Unlock	fa90aedfedefe5
... Lots of presses
Lock	fa90aeefcb3b23
```
* Looking through the codes its easy to see that the 7th hex digit corresponds exactly to the type of code sent.
   * 0x4 = Trunk
   * 0xd = Unlock
   * 0xe = Lock
* The 8th through 12th hex nibbles appear to decrement every code pressed.
* The last byte appears to be random, but there has to be some pattern to it...

### Last Byte Calcuation
I assume the last byte was a checksum or some other cryptographic means of securing the transmission and preventing attacks like the one I'm trying to accomplish.   I started by looking for patterns in the value of the last byte of data.  There seems to be pattern in the last nibble of data but the second to last nibble appears purely random over the dataset that I have.  I then looked into a stndard XOR checksum.  I started by xoring all bytes except the checksum byte to see if I could see a pattern.  I quickly noticed that the inverse of the checksum I calculated was pretty close to the checksum byte.  After inverting all checksums I noticed that we are off by just 1.  I then added 1 before inverting the xor and figured out the "security" checksum.
```
                      xor of all bytes 
                        except last           inverted       ~(xor + 1)
Unlock	fa90aedfee575c      0xa2                0x5d            0x5c
Unlock	fa90aedfee565b      0xa3                0x5c            0x5b
Unlock	fa90aedfee555e      0xa0                0x5f            0x5e
Unlock	fa90aedfee545d      0xa1                0x5e            0x5d
Lock	fa90aeefee5368      0x96                0x69            0x68
Trunk	fa90ae4fee52c7      0x37                0xc8            0xc7
Trunk	fa90ae4fee51ca      0x34                0xcb            0xca
Unlock	fa90aedfee5059      0xa5                0x5a            0x59
Unlock	fa90aedfee4f44      0xba                0x45            0x44
Unlock	fa90aedfee4e43      0xbb                0x44            0x43
Unlock	fa90aedfee4d46      0xb8                0x47            0x46
Unlock	fa90aedfee4c45      0xb9                0x46            0x45
Unlock	fa90aedfee4b40      0xbe                0x41            0x40
Unlock	fa90aedfee4a3f      0xbf                0x40            0x3f
Unlock	fa90aedfee4942      0xbc                0x43            0x42
Unlock	fa90aedfee4841      0xbd                0x42            0x41
Lock	fa90aeefee477c      0x82                0x7d            0x7c
Lock	fa90aeefee467b      0x83                0x7c            0x7b
Lock	fa90aeefee457e      0x80                0x7f            0x7e
Lock	fa90aeefee447d      0x81                0x7e            0x7d
Lock	fa90aeefee4378      0x86                0x79            0x78
Lock	fa90aeefee4277      0x87                0x78            0x77
Lock	fa90aeefee417a      0x84                0x7b            0x7a
Lock	fa90aeefee4079      0x85                0x7a            0x79
Lock	fa90aeefcb3b23      0xdb                0x24            0x23
```

The script [decode_codes.py](/subaru/scripts/decode_codes.py) was written to validate the checksums and the ability to calculate the next code.  The script can be run run py doing `python decode_codes.py example_codes.txt` the example_codes.txt file contains the large list from above and can be used to validate the checksum and next code algorithms.  The `decode_codes.py` script checks the checksum of the curren code to validate it,  it then gets the next code and compares it to the next code found in the text file.   I also wrote [inverted_decode_codes.py](/subaru/scripts/inverted_decode_codes.py) to determine how the checksum is calculated if I had interpreted the manchester data incorrectly.

### Packet Decoded
With the knowledge from above we can markup our decoded packet further

![ScreenShot](/subaru/Examples/decoded_packet_marked_up.png)

# gnuradio
I wrote a gnuradio block that runs a packet parser looking for keyless entry packets. The code can be found at [subaru_remote_decode_char_impl.cc](/subaru/gnuradio/custom_blocks/gr-subaru_remote/lib/subaru_remote_decode_char_impl.cc).  That block is used in a gnuradio flow chart at [rtl_sdr_recording_to_packets.grc](/subaru/gnuradio/rtl_sdr_recording_to_packets/rtl_sdr_recording_to_packets.grc).  That flowchart is capable of decoding recordings made via the `rtl_sdr` command or also decoding live data from the rtl_sdr source block.  
The custom block can be compiled and gnuradio-companion launched by running `make` in gnuradio/custom_blocks

# CC1101 Radio
The [TI cc1101](http://www.ti.com/lit/ds/symlink/cc1101.pdf) Sub-1 GHz RF Transceiver should be capable of transmitting and receiving in the 315MHz band and decoding the keyless entry packets.  The cc1101's sister product the cc1111 is used in the [rfcat](https://github.com/atlas0fd00m/rfcat) project and could likely be used here too.  I have some [panStamp AVR 2](https://github.com/panStamp/panstamp/wiki/panStamp%20AVR%202.-Technical%20details) modules that include a Atmega328p and a TI cc1101.  I will use this for the development platform for receiving, modifying and unlocking a car.  Although the data is manchester encoded and it appears that the preamble is manchester encoded also there is a "break" bit at end of the preamble(pointed to in the above figure as the "first manchester bit".  If the cc1101 had manchester mode enabled I believe it would fail on this bit and reject the packet.  So I decided that I would disable manchester encoding/decoding on the cc1101 and just have to post convert the packets after reception.  

![ScreenShot](/subaru/Examples/decoded_packet_sync_word.png)

I used the panstamp codebase as a start since it had the libraries(spi, cc1101) that I needed.  I wanted to be able to build outside arduino since I didn't need any of the special features so I ported the libraries to "raw gcc" and added a simple uart library.  I used smartrf studio to determine new register settings, and added a patable_OOK function to fill in the patable for OOK transmission.

There are 3 applications which can be run on the panstamp board.
* remote_sender
   * This applicaton was written to refine the transmission of packets and ensure they matched what the key fob was sending out
   * A single transmission is sent for each keypress sent over the serial terminal.
* remote_receiver
   * This applicaton was written to validate receiving the packet on the cc1101 hardware.  It prints out each key fob transmission received
* remote_prevent_locker
   * This program is the example code for the actual attack.
   * The program waits to receive a key fob transmission.  Then pauses 1 second and sends an unlock command.  If this application is running while someone is trying to lock their car they will have one second before the car unlocks due to the transmission sent.
