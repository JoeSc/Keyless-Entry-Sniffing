from __future__ import print_function
import sys


class InvertedPacket():
    code_types = {"unlock": 0x2, "lock": 0x1, "trunk":0xb}

    def __init__(self, packet_ary=None, packet_str=None):
        if packet_ary and packet_str:
            raise Exception("Specify only one of packet_ary and packet_str")
        elif packet_ary:
            self.packet_ary = packet_ary
        elif packet_str:
            self.packet_ary = self.decode_packet_str(packet_str)
        else:
            raise Exception("How did we get here")

    def hex_str(self):
        packet_str = "".join(["%02x"%x for x in self.packet_ary])[0:]
        return packet_str

    def get_addr(self):
        return "".join(["%02x"%x for x in self.packet_ary[0:3]])[0:]

    def get_roll(self):
        return "".join(["%02x"%x for x in self.packet_ary[3:6]])[1:]

    def get_code(self):
        return "".join(["%02x"%x for x in self.packet_ary[3:4]])[0]

    def get_checksum(self):
        return "".join(["%02x"%x for x in self.packet_ary[6:7]])

    def verbose_str(self):
        return "%s  addr=%s  code=%s  roll=%s  cksum=%s"%(self, self.get_addr(), self.get_code(), self.get_roll(), self.get_checksum())


    def __repr__(self):
        checksum = "OK" if self.checksum_ok() else "FAIL"
        packet_type = self.get_packet_type()
        return "%s  %s  %6s"%(self.hex_str(), checksum, packet_type)

    def decode_packet_str(self,pkt_str):
        pkt = int(pkt_str, 16)
        lst = []
        for i in range(7):
            lst.insert(0, int((pkt >> (i*8)) & 0xff))
        return lst

    def get_packet_type(self):
        type_nibble = self.packet_ary[3] >> 4
        # Invert the dictionary to lookup type with nibble value
        inv_code_types = {v: k for k, v in self.code_types.items()}
        return inv_code_types[type_nibble]

    def foo(self):
        code_ary = self.packet_ary
        checksum = 0
        for x in code_ary[0:-1]:
            checksum ^= x
        checksum += 1
        checksum = checksum & 0xff
        return hex(checksum)

    def calc_checksum(self, code_ary=None):
        if not code_ary:
            code_ary = self.packet_ary
        checksum = 0
        for x in code_ary[0:-1]:
            checksum ^= x
        checksum += 1
        checksum = checksum & 0xff
        return checksum

    def checksum_ok(self):
        return self.packet_ary[-1] == self.calc_checksum(self.packet_ary)

    def _set_checksum(self, new_pkt):
        new_pkt[-1] = self.calc_checksum(new_pkt)
        return new_pkt
    
    def get_next_code(self, pkt_type="unlock"):
        type_byte = self.packet_ary[3] & 0x0f
        type_byte = type_byte | (self.code_types[pkt_type]<< 4)
        self.packet_ary[3] = type_byte
        count = (self.packet_ary[-3] << 8) + self.packet_ary[-2]
        count = count + 1
        self.packet_ary[-3] = count >> 8
        self.packet_ary[-2] = count & 0xff
        return InvertedPacket(packet_ary=self._set_checksum(self.packet_ary))



if __name__ == "__main__":
    f = open(sys.argv[1])
    fl = f.readlines()
    for i in range(len(fl)):
        if "#" in fl[i]:
            continue
        cur_type, cur_code = fl[i].split()
        cur_code = "%014x"%((~int(cur_code, 16)) & 0xfffffffffffff)
        p = InvertedPacket(packet_str=cur_code)
        print("rx = %s   "%p.verbose_str(), end = "    ")
        try:
            if "#" in fl[i+1]:
                print("\n#")
                continue
            next_type, next_code = fl[i + 1].split()
            next_code = "%014x"%((~int(next_code, 16)) & 0xfffffffffffff)
            next_packet = p.get_next_code(pkt_type=next_type.lower())
            if next_packet.hex_str() == next_code:
                print("MATCH OK")
            else:
                print("MATCH FAILURE")
        except IndexError:
            # End of file
            print()
