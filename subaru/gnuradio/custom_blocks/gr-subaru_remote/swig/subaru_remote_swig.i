/* -*- c++ -*- */

#define SUBARU_REMOTE_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "subaru_remote_swig_doc.i"

%{
#include "subaru_remote/subaru_remote_decode_char.h"
%}

%include "subaru_remote/subaru_remote_decode_char.h"
GR_SWIG_BLOCK_MAGIC2(subaru_remote, subaru_remote_decode_char);
