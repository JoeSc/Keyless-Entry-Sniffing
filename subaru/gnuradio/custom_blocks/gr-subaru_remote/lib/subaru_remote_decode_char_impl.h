/* -*- c++ -*- */
/* 
 * Copyright 2018 <+YOU OR YOUR COMPANY+>.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_SUBARU_REMOTE_SUBARU_REMOTE_DECODE_CHAR_IMPL_H
#define INCLUDED_SUBARU_REMOTE_SUBARU_REMOTE_DECODE_CHAR_IMPL_H

#include <subaru_remote/subaru_remote_decode_char.h>
#include <math.h>
#include <inttypes.h>

namespace gr {
  namespace subaru_remote {

    class subaru_remote_decode_char_impl : public subaru_remote_decode_char
    {
     private:
        double sample_rate;
        double bit_time;
        uint32_t sample;
        uint32_t last_transition;
        uint8_t last_level;
        enum states {state_waiting, state_start, state_sync, state_preamble, state_data}packet_state;
        uint32_t transitions[1024];
        uint16_t transition_cnt;
        uint64_t packet_data;
        uint8_t packet_bit_cnt;
        uint32_t packet_number;
        double clk_period;

     public:
      subaru_remote_decode_char_impl(double samp_rate);
      ~subaru_remote_decode_char_impl();

      // Where all the action really happens
      int work(int noutput_items,
         gr_vector_const_void_star &input_items,
         gr_vector_void_star &output_items);
      uint8_t *pkt_int_to_ary(uint64_t code);
      uint8_t calc_checksum(uint64_t code);
      uint8_t checksum_ok(uint64_t code);
    };

  } // namespace subaru_remote
} // namespace gr

#endif /* INCLUDED_SUBARU_REMOTE_SUBARU_REMOTE_DECODE_CHAR_IMPL_H */

