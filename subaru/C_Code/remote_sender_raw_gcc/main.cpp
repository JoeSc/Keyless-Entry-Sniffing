extern "C"
{
	#include "uart.h"
}
#include <stdio.h>
#include "cc1101.h"
#include <avr/io.h>
#include <util/delay.h>


#define LED_DDR  DDRB
#define LED_PORT PORTB
#define LED      PINB5

CC1101 radio;
CCPACKET tx_packet;

#define PKTLEN (15)

/* Turn a 64-bit value of the packet into an array of ints
 * This functions handles converting the binary data to manchester encoded
 * data.
 */
uint8_t * hex_to_packetdata(uint64_t hexp)
{
	uint8_t man_bit_0, man_bit_1, k, data_bit = 0;
	int i;
	static uint8_t data[PKTLEN] = {0};

	for (i = 0; i < PKTLEN; i++)
		data[i] = 0;

	/* 73 should be a div round down or something */
	for (i = 110; i >= 0; i-= 2) {
		k = i;
		data_bit = hexp & 1;
		hexp = hexp >> 1;

		if (data_bit) {
			man_bit_0 = 1;
			man_bit_1 = 0;
		} else {
			man_bit_0 = 0;
			man_bit_1 = 1;
		}

		data[k/8] |= (man_bit_1 << (7 - (k % 8)));
		k+=1;
		data[k/8] |= (man_bit_0 << (7 - (k % 8)));
	}

	return data;
}

void send_remote_code(uint64_t code)
{
	int start = 0;

	/* Generate the super long preamble for the first transmission */
	for (start = 0; start < 132; start ++)
		tx_packet.data[start] = 0xCC;

	/* Create the sync bit */
	tx_packet.data[start++] = 0xCD;

	/* Create the sync bit */
	uint8_t *txp = hex_to_packetdata(code);
	for ( int k = 0; k < PKTLEN; k++)
		tx_packet.data[start++] = txp[k];


	/* Set the packet length then send it out */
	tx_packet.length = start;
	radio.sendData(tx_packet);
	_delay_ms(1);

	/* The second and third transmission contain much smaller preambles
	 * Adjust the data sent to accomodate that
	 */
	start = 15;
	tx_packet.data[start++] = 0xCD;
	for ( int k = 0; k < PKTLEN; k++)
		tx_packet.data[start++] = txp[k];

	/* Send second and third packets */
	tx_packet.length = start;
	radio.sendData(tx_packet);
	_delay_ms(1);
	radio.sendData(tx_packet);
}


void loop() {
	printf("Sending packets\n");
	send_remote_code(0xfa90aeefcb3b23ULL);
	puts("Any key to send again");
	getchar();

}

int main(void) {
	uart_init();
	stdin = stdout = fdevopen(uart_putchar, uart_getchar);

	/* set LED pin as output */
	LED_DDR |= _BV(LED);
	LED_PORT &= ~_BV(LED);

	puts("Hello world!\n");

	radio.init();
	puts("Initializing Radio\n");
	for(int i = 0; !uart_get_available(); i++) {
		if ((i % 10) == 0)
			puts("Starting on button press");
		_delay_ms(100);
	}
	while (1)
		loop();
	return 0;
}
