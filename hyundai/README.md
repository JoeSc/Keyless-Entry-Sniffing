Hyundai RKE Hacking
=====================================

# Motivation
After looking into Subaru keyless entry I wanted to take a look at some other cars available to me.  First up was a 2015 Hyundai Santa fe

## Summary
While this information is preliminary and I need to capture more remote key presses I believe it to be correct.  The key fob uses a 12-bit rolling code with a 4-bit parity code.  There does not appear to be any other security included in the transmission so this key fob should be simple to clone and spoof like the Subaru ones.

## More information
Looking at the [TQ8-RKE-3F04 key fob User Manual](https://fccid.io/TQ8-RKQ8-Q8--3F04/Users-Manual/Users-Manual-1662171) on the FCC website indicates that it should have the same vulnerability as the Subaru remotes.

* recordings/hyundai_remote_recording_fsk_demod.wav was created by using gnuradio/rtl_sdr_recording_to_wav/rtl_sdr_recording_to_wav.grc which just fm demodulates the recordings/hyundai_remote_recording.c8 file.
* Then we can run `wav_to_packets.py`
```bash
$ python wav_to_packets.py ../recordings/hyundai_remote_recording_fsk_demod.wav
GOOD Packet Number  0 @   182040  = d641452b19aeb 52
GOOD Packet Number  1 @   188991  = d641452b19aeb 52
GOOD Packet Number  2 @   438988  = d641452b29be9 52
GOOD Packet Number  3 @   445939  = d641452b29be9 52
GOOD Packet Number  4 @   710524  = d641452b19ced 52
GOOD Packet Number  5 @   717475  = d641452b19ced 52
GOOD Packet Number  6 @   882096  = d641452b29def 52
GOOD Packet Number  7 @   889047  = d641452b29def 52
GOOD Packet Number  8 @  1049234  = d641452b19eef 52
GOOD Packet Number  9 @  1056185  = d641452b19eef 52
GOOD Packet Number 10 @  1148722  = d641452b19fe0 52
GOOD Packet Number 11 @  1155673  = d641452b19fe0 52
```
* Those packets are put into example_codes.txt
* Those packets can be checked with the `decode_codes` script
   * The below script has been marked up with the names from the [TQ8-RKE-3F04 key fob User Manual](https://fccid.io/TQ8-RKQ8-Q8--3F04/Users-Manual/Users-Manual-1662171)
```bash
 $ python decode_codes.py example_codes.txt
     user    id   function   rolling   parity    checksum?
rx = d6    41452b    1        9ae        b          OK
rx = d6    41452b    1        9ae        b          OK
rx = d6    41452b    2        9be        9          OK
rx = d6    41452b    2        9be        9          OK
rx = d6    41452b    1        9ce        d          OK
rx = d6    41452b    1        9ce        d          OK
rx = d6    41452b    2        9de        f          OK
rx = d6    41452b    2        9de        f          OK
rx = d6    41452b    1        9ee        f          OK
rx = d6    41452b    1        9ee        f          OK
rx = d6    41452b    1        9fe        0          OK
rx = d6    41452b    1        9fe        0          OK
```
* The user and id code is static across my recordings
* The function is varies between 1 and 2 for locks/unlocks
* It appears that the rolling code adds 0x10 each roll.  More captures are needed
* The checksum is xoring all the nibbles except the last together and then adding 1
    * Using the last packet from above
    * 0xd ^ 0x6 ^ 0x4 ^ 0x1 ^ 0x4 ^ 0x5 ^ 0x2 ^ 0xb ^ 0x1 ^ 0x9 ^ 0xf ^ 0xe = 0xf
    * 0xf + 1 = 0x0
