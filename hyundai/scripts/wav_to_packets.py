import wave
import sys
from collections import deque
import logging
import math
#import progressbar
from struct import unpack

logging.basicConfig()
statelog = logging.getLogger("STATE")
statelog.setLevel(logging.WARNING)

log = logging.getLogger("")
log.setLevel(logging.WARNING)

#states = open("states.txt", "w")


def packet2hexstr(packet):
    bit_string = ''
    for bit in packet:
        bit_string += str(bit)
    return "%x"%int(bit_string,2)


sample_rate = 0
man_clk = 500E-6

def signal_gen():
    global sample_rate
    f = wave.open(sys.argv[1])
    samplesize = f.getsampwidth() * 8
    #sample_rate = float(f.getframerate()) / 20
    sample_rate = float(f.getframerate())
    channels = f.getnchannels()
    for samp in range(f.getnframes()):
        frame = f.readframes(1)
        mag = None
        if f.getsampwidth() == 1:
            mag, = unpack('B', frame)
            if mag < 128:
                mag = 0
            else:
                mag = 1
            #print("a dec = %6d"%mag)
            #mag =  (mag / 256.)*2 - 1
            #print("b dec = %6d"%mag)
        elif f.getsampwidth() == 2:
            mag, = unpack('<h', frame) 
            #print("dec = %6d"%mag, end ="  ")
            mag /= 32767.
            if mag < 0:
                mag = 0
            else:
                mag = 1
    
        #states.write("%d   %d   %d\n"%(samp, mag, round(mag)))
        yield samp, mag, mag
#        if samp % 20 == 0:
#            states.write("%d   %d\n"%(samp/20, round(mag)))
#            yield samp/20, mag, round(mag)

last_signal_val = 0
state = "waiting"
transitions = []
packet_number = 0
bin_data = []
man_data = []
man_str = ""
pre_data = ""

search_transitions = deque([0]*100, maxlen = 10)
last_signal_transition = 0

for samp, mag, signal_val in signal_gen():

    #if state is "waiting":
    #    state_val = 0
    #elif state is "start":
    #    state_val = 1
    #elif state is "sync":
    #    state_val = 2
    #elif state is "preamble":
    #    state_val = 3
    #elif state is "data":
    #    state_val = 4
    #else:
    #    state_val = 99

    #states.write("%d    %.3f    %d    %d  %.3f\n"%(samp, mag, signal_val, state_val, avg))

    transition = signal_val - last_signal_val
    last_signal_val = signal_val

    if transition != 0:
        tran_time = samp - last_signal_transition
        last_signal_transition = samp
        #log.info("%d  TRANSITION %d"%(samp, transition))


    if state is "waiting":
        if transition != 0:
            search_transitions.append(tran_time)
            # To save time here lets just check that this most recent tran_time was right, if so then do the calcs, should shave of a lot of computation
            if 1.8 * man_clk < tran_time/sample_rate < 2.2 * man_clk:
                #print("%d  "%samp, search_transitions)
                log.info("%d  TRAN TIME GOOD, checking"%(samp))
                tran_ok = all( 1.9 * man_clk < x/sample_rate < 2.1 * man_clk for x in search_transitions)
                #avg_tran = sum(search_transitions) / float(len(search_transitions))
                #log.info("    avg_tran = %d, checking"%(avg_tran))
                #if 1900 < avg_tran < 2100:
                if tran_ok:
                    state = "preamble"
                    log.info("%d entering PREAMBLE!"%(samp))
                    transitions.append(samp)

    #elif state is "preamble":
    #    if transition != 0:
    #        transitions.append(samp)
    #    if transition == -1:
    #        time_since = transitions [-1] - transitions[-2]
    #        log.info("%d    time_since = %d"%(samp, time_since))
    #        if 0.75*1000 < time_since < 1.25*1000:
    #            log.info("%d entering DATA!"%(samp))
    #            state = "data"

    elif state is "preamble" or state is "data":
        if state is "preamble":
            if "011" in man_str:
                man_str = "1"
                log.info("%d entering DATA!"%(samp))
                state = "data"
        time_since = (samp - transitions[-1])
        if transition != 0:
            log.info("%d    time_since = %d"%(samp, time_since))
            if 0.75 * man_clk < time_since / sample_rate < 1.49 * man_clk:
                pass
                # Non data transition
            elif 1.51*man_clk < time_since/sample_rate < 2.49*man_clk:
                # data transition
                transitions.append(samp)
                if transition == 1:
                    man_data.append(0)
                    man_str += '0'
                    log.info("%d Got 0", samp)
                else:
                    man_data.append(1)
                    man_str += '1'
                    log.info("%d Got 1", samp)
            else:
                log.error("%d ISSUE DECODING MAN DATA"%(samp))
        elif time_since/sample_rate > 2.5 * man_clk:
                log.info("Lost Sync @ %d"%samp)
                print("BAD  Packet Number %2d @ %8d  = %x %d"%(packet_number, transitions[-1], int(man_str, 2), len(man_str)))
                packet_number += 1
                state = "waiting"
                del man_data[:]
                man_str = ""
                for _ in range(10):
                    search_transitions.appendleft(0)
        elif state is "data" and len(man_str) == 52:
                print("GOOD Packet Number %2d @ %8d  = %x %d"%(packet_number, transitions[-1], int(man_str, 2), len(man_str)))
                packet_number += 1
                state = "waiting"
                del man_data[:]
                man_str = ""
                for _ in range(10):
                    search_transitions.appendleft(0)

