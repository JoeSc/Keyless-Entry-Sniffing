from __future__ import print_function
import sys


class Packet():
    Unlock = 0xd
    Lock = 0xe
    Trunk = 0x4
    def __init__(self, packet_ary=None, packet_str=None):
        if packet_ary and packet_str:
            raise Exception("Specify only one of packet_ary and packet_str")
        elif packet_ary:
            self.packet_ary = packet_ary
        elif packet_str:
            self.packet_ary = self.decode_packet_str(packet_str)
        else:
            raise Exception("How did we get here")

    def __repr__(self):
        packet_int = int("".join(["%02x"%x for x in self.packet_ary])[0:], 16)
        user_code = (packet_int >> (52-8)) & 0xff
        id_code = (packet_int >> (52-32)) & 0xffffff
        func_code = (packet_int >> (52-36)) & 0xf
        roll_code = (packet_int >> (52-48)) & 0xfff
        parity_code = (packet_int >> (52-52)) & 0xf
        checksum = "OK" if self.checksum_ok() else "FAIL"

        return "%02x  %06x  %01x  %03x  %01x     %s"%(user_code, id_code, func_code, roll_code, parity_code, checksum)
        return "%x"%(packet_int)
        #packet_str = "".join(["%02x"%x for x in self.packet_ary])[0:]
        #packet_type = self.get_packet_type()
        #return "%s %s %6s"%(packet_str, checksum, packet_type)

    def decode_packet_str(self,pkt_str):
        pkt = int(pkt_str, 16)
        lst = []
        for i in range(7):
            lst.insert(0, int((pkt >> (i*8)) & 0xff))
        return lst

    def get_packet_type(self):
        type_nibble = self.packet_ary[4] & 0xf
        return str(type_nibble)
        if type_nibble == self.Unlock:
            return "Unlock"
        elif type_nibble == self.Lock:
            return "Lock"
        elif type_nibble == self.Trunk:
            return "Trunk"
        else:
            return "Unknown"

    def calc_checksum(self, code_ary=None):
        if not code_ary:
            code_ary = self.packet_ary
        checksum = 0
        for x in code_ary[0:-1]:
            checksum ^= x >> 4
            checksum ^= x & 0xf
        checksum ^= code_ary[-1] >> 4
        checksum += 1
        checksum = checksum & 0xf
        return checksum

    def checksum_ok(self):
        return self.packet_ary[-1] & 0xf == self.calc_checksum(self.packet_ary)

#    def _set_checksum(self, new_pkt):
#        new_pkt[-1] = self.calc_checksum(new_pkt)
#        return new_pkt
#    
#    def get_next_code(self, pkt_type=Unlock):
#        type_bytee = self.packet_ary[3] & 0x0f
#        type_bytee = type_bytee | (pkt_type << 4)
#        self.packet_ary[3] = type_bytee
#        count = (self.packet_ary[-3] << 8) + self.packet_ary[-2]
#        count = count - 1
#        self.packet_ary[-3] = count >> 8
#        self.packet_ary[-2] = count & 0xff
#        return Packet(packet_ary=self._set_checksum(self.packet_ary))




if __name__ == "__main__":
    code_list = []
    next_packet = 0
    
    f = open(sys.argv[1])
    for x in f.readlines()[0:]:
        if "#" in x:
            continue
        p = Packet(packet_str=x)
        print("rx = %s   "%str(p))
